﻿using System;
using Game.Components;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public sealed class GameController:MonoBehaviour
    {
        [SerializeField] private PizzaComponent _pizza;
        [SerializeField] private ElementFactory _factory;
        [SerializeField] private Board _board;
        [SerializeField] private Text _scoreText;
        [SerializeField] private Button _menuButton;

        private int _score;

        private void Awake()
        {
            _pizza.ItemCollided += PizzaOnItemCollided;
            _menuButton.onClick.AddListener(OnMenuButtonClicked);
        }

        private void PizzaOnItemCollided(Element element)
        {
            _score += element.Model.Cost;
            _scoreText.text = _score.ToString();
            _scoreText.color = _score > 0 ? new Color(0, 1, 0, 1) : new Color(1, 0, 0, 1);
        }

        private void OnMenuButtonClicked()
        {
            _factory.Restart();
            _board.Restart();
            _pizza.Restart();
            _score = 0;
            _scoreText.text = _score.ToString();
            _scoreText.color = new Color(0, 0, 0, 1);
        }
    }
}