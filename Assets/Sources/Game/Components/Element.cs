﻿using System;
using System.Collections;
using Game.ScriptableObjects;
using UnityEngine;

namespace Game.Components
{
    public sealed class Element:MonoBehaviour
    {
        public const string Tag = "Element";

        public string Id { get; private set; }

        [SerializeField]
        private ElementModel _model;

        private ElementFactory _factory;

        public ElementModel Model
        {
            get { return _model; }
        }

        public void Initialize(ElementFactory factory)
        {
            Id = Guid.NewGuid().ToString();
            _factory = factory;
            StartCoroutine(PositionChecker());
        }

        private IEnumerator PositionChecker()
        {
            while (transform.position.y > -50)
            {
                yield return new WaitForFixedUpdate();
            }
            DestroyElement();
        }

        public void DestroyElement()
        {
            _factory.Remove(Id);
            Destroy(gameObject);
        }
    }
}