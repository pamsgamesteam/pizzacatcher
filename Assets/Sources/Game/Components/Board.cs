﻿using Game.Tools;
using UnityEngine;

namespace Game.Components
{
    public sealed class Board : MonoBehaviour
    {
        #region ROTATE 
        [SerializeField]
        private float _sensitivity = 1f;
        private Vector3 _mouseReference;
        private Vector3 _mouseOffset;
        private Vector3 _rotation = Vector3.zero;
        private bool _isRotating;

        #endregion

        private void FixedUpdate()
        {
            if (!_isRotating) return;
            
            _mouseOffset = (Input.mousePosition - _mouseReference);

            _rotation.z = (_mouseOffset.x + _mouseOffset.y) * _sensitivity;
            if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x < 0)
                _rotation.z *= -1;

            var angle = (transform.eulerAngles.z + _rotation.z).ClampAngle(-15, 15);

            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y,angle);
            _mouseReference = Input.mousePosition;
        }

        private void OnMouseDown()
        {
            // rotating flag 
            _isRotating = true;
            // store mouse position 
            _mouseReference = Input.mousePosition;
        }

        private void OnMouseUp()
        {
            // rotating flag 
            _isRotating = false;
        }

        public void Restart()
        {
            _isRotating = false;
            transform.eulerAngles = Vector3.zero;
        }
    }
}