﻿using System;
using UnityEngine;

namespace Game.Components
{
    public sealed class PizzaComponent:MonoBehaviour
    {
        public event Action<Element> ItemCollided;

        private void OnCollisionEnter2D(Collision2D col)
        {
            if(col.gameObject.tag!=Element.Tag)
                return;

            var element = col.gameObject.GetComponent<Element>();
            if (ItemCollided != null)
            {
                ItemCollided.Invoke(element);
            }

            element.DestroyElement();
        }

        public void Restart()
        {
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;

            Rigidbody2D rgb = GetComponent<Rigidbody2D>();
            rgb.velocity = Vector2.zero;
            rgb.angularVelocity = 0;
        }
    }
}