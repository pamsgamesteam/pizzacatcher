﻿using UnityEngine;

namespace Game.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NewElement", menuName = "Game/Element")]
    public sealed class ElementModel:ScriptableObject
    {
        [SerializeField]
        [Range(-100, 100)]
        public int Cost;
    }
}