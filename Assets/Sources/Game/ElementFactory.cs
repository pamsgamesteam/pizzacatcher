﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Components;
using UnityEngine;

namespace Game
{
    public sealed class ElementFactory:MonoBehaviour
    {
        [SerializeField] private float _minWait = 0.3f;
        [SerializeField] private float _maxWait = 0.7f;

        [SerializeField] private List<Element> _elementsPrefab;
        private Dictionary<string,Element> _elements = new Dictionary<string, Element>();

        private void Start()
        {
            StartCoroutine(SpawnElements());
        }

        private IEnumerator SpawnElements()
        {
            while (true)
            {
                var position = transform.position;
                position.x += Random.Range(-5, 5);
                var element = Instantiate(_elementsPrefab[Random.Range(0, _elementsPrefab.Count)],position,Quaternion.identity);
                element.Initialize(this);
                _elements.Add(element.Id,element);
                yield return new WaitForSeconds(Random.Range(_minWait,_maxWait));
            }
        }

        public void Remove(string id)
        {
            if(_elements.ContainsKey(id))
                _elements.Remove(id);
        }

        public void Restart()
        {
            var values = _elements.Values.Where(t=>t!=null).ToList();
            foreach (var element in values)
            {
                    element.DestroyElement();
            }
        }
    }
}